class Address < ApplicationRecord
  ZIP_CODE_REGEXP = /\A\d{2}-\d{3}\z/

  belongs_to :addressable, polymorphic: true
  validates :street, presence: true, if: :for_user?
  validates :city, presence: :true, if: :for_user?
  validates :zip_code, presence: :true, if: :for_user?
  validates :zip_code, format: { with: ZIP_CODE_REGEXP }, allow_blank: true
  validates :country, presence: :true, if: :for_user?

  private
  def for_user?
    addressable.class == User
  end

end

