class Company < ApplicationRecord
  has_one :address, as: :addressable
  belongs_to :user

  accepts_nested_attributes_for :address
  validates :name, length: { maximum: 200 }, allow_blank: true
end
