class User < ApplicationRecord
  PHONE_REGEXP = /\A\d{9}\z/
  DATE_REGEXP = /\A\d{4}-\d{2}-\d{2}\z/
  EMAIL_REGEXP = URI::MailTo::EMAIL_REGEXP

  has_one :address, as: :addressable
  has_one :company

  accepts_nested_attributes_for :company
  accepts_nested_attributes_for :address

  validates :first_name, presence: true, length: { maximum: 100 }
  validates :last_name, presence: true, length: { maximum: 100 }
  validates :email, presence: true, format: { with: EMAIL_REGEXP }
  validates :phone_number, format: { with: PHONE_REGEXP }, allow_blank: true
  validate :date_format_correctness
  validate :date_of_birth_cannot_be_in_the_future

  private
  def date_format_correctness
    if date_of_birth_before_type_cast.present?
      is_proper_date = date_of_birth_before_type_cast =~ DATE_REGEXP
      errors.add(:date_of_birth, "is invalid") unless is_proper_date
    end
  end

  def date_of_birth_cannot_be_in_the_future
    if date_of_birth.present? && date_of_birth > Date.today
      errors.add(:date_of_birth, "can't be in the future")
    end
  end
end