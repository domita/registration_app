module UsersHelper
  def display_form_errors(user)
    raw(user.errors.full_messages.join("</br>"))
  end
  def country_options
    Country.all
  end
end


