class UsersController < ApplicationController
  def new
    @user = User.new
    @user.build_address
    @user.build_company
    @user.company.build_address
  end

  def create
    @user = User.new(create_params)
    if @user.save
      redirect_to new_user_path
    else
      render :new
    end
  end

  private
  def create_params
    params.require(:user).permit(
      :first_name,
      :last_name,
      :email,
      :date_of_birth,
      :phone_number,
      company_attributes: [:name, address_attributes: [:street, :city, :zip_code, :country]],
      address_attributes: [:street, :city, :zip_code, :country]
    )
  end
end
