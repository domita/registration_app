require 'rails_helper'

def fill_registration_form
  within("#new_user") do
    fill_in 'First name:', with: 'Jan'
    fill_in 'Last name:', with: 'Kowalski'
    fill_in 'Email:', with: 'jan@wp.pl'
    fill_in 'Date of birth (yyyy-mm-dd):', with: '1990-01-12'
    fill_in 'Phone number:', with: '711001100'
    fill_in 'Name:', with: 'Comp'
  end
  within(".user_address") do
    fill_in 'Street:', with: 'Kolorowa'
    fill_in 'City:', with: 'Warszawa'
    fill_in 'Zip code:', with: '00-357'
    select('Poland', :from => 'Country')
  end
  within(".company_address") do
    fill_in 'Street:', with: 'Batorego'
    fill_in 'City:', with: 'Katowice'
    fill_in 'Zip code:', with: '40-001'
    select('Poland', :from => 'Country')
  end
end

feature "Filling registration form", type: :feature do
  background do
    visit new_user_path
  end

  feature "with mandatory fields given" do
    background do
      fill_registration_form
    end

    scenario "creates user" do
      expect { click_button "Save" }.to change { User.count }.by(1)
      user = User.last
      expect(user.first_name).to eq('Jan')
      expect(user.last_name).to eq('Kowalski')
      expect(user.email).to eq('jan@wp.pl')
      expect(user.date_of_birth.to_s).to eq('1990-01-12')
      expect(user.phone_number).to eq('711001100')
    end

    scenario "creates user address" do
      user_addresses = Address.where(addressable_type: 'User')
      expect { click_button "Save" }.to change { user_addresses.count }.by(1)
      user_address = user_addresses.last
      expect(user_address.street).to eq('Kolorowa')
      expect(user_address.city).to eq('Warszawa')
      expect(user_address.zip_code).to eq('00-357')
      expect(user_address.country).to eq('Poland')
    end

    scenario "creates company" do
      expect { click_button "Save" }.to change { Company.count }.by(1)
      company = Company.last
      expect(company.name).to eq("Comp")
      expect(company.user.last_name).to eq("Kowalski")
    end

    scenario "creates company address" do
      company_addresses = Address.where(addressable_type: 'Company')
      expect { click_button "Save" }.to change { company_addresses.count }.by(1)
      company_address = company_addresses.last
      expect(company_address.street).to eq('Batorego')
      expect(company_address.city).to eq('Katowice')
      expect(company_address.zip_code).to eq('40-001')
      expect(company_address.country).to eq('Poland')
    end
  end

  feature "with missing mandatory fields" do
    scenario "shows validation messages" do
      click_button "Save"
      expect(page).to have_content "Address street can't be blank"
      expect(page).to have_content "Address city can't be blank"
      expect(page).to have_content "Address zip code can't be blank"
      expect(page).to have_content "Address country can't be blank"
      expect(page).to have_content "First name can't be blank"
      expect(page).to have_content "Last name can't be blank"
      expect(page).to have_content "Email can't be blank"
      expect(page).to have_content "Email is invalid"
    end

    scenario "does not create user" do
      expect { click_button "Save" }.to_not change { User.count }
    end
  end
end



