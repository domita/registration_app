require 'rails_helper'

describe Company do

  context 'name is 201 characters' do
    subject do
      described_class.new(name: "a" * 201)
    end
    it "is not valid" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Name is too long (maximum is 200 characters)")
    end
  end

end