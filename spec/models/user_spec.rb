require 'rails_helper'

describe User do

  context 'first_name is 101 characters' do
    subject do
      described_class.new(first_name: "a" * 101)
    end
    it "is not valid" do
      expect(subject).to_not be_valid #expect(subject.valid?).to eq(false)
      expect(subject.errors.full_messages).to include("First name is too long (maximum is 100 characters)")
    end
  end

  context 'email' do
    subject do
      described_class.new(email: "testtest%")
    end
    it "is not valid" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Email is invalid")
    end
  end

  context 'email' do
    subject do
      described_class.new(first_name: "Jan", last_name: "Kowalski", email: "test@wp.pl")
    end
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context 'date_of_birth' do
    subject do
      described_class.new(date_of_birth: "abc12345")
    end
    it "is not valid" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Date of birth is invalid")
    end
  end

  context 'date_of_birth' do
    subject do
      described_class.new(date_of_birth: "2050-03-12")
    end
    it "is in the future" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Date of birth can't be in the future")
    end
  end

  context 'date_of_birth' do
    subject do
      described_class.new(first_name: "Jan", last_name: "Kowalski", email: "jan@wp.pl", date_of_birth: "2014-11-03")
    end
    it "is valid" do
      expect(subject).to be_valid
    end
  end

  context 'phone number' do
    subject do
      described_class.new(phone_number: "abc12345")
    end
    it "is not valid" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Phone number is invalid")
    end
  end

  context 'phone number' do
    subject do
      described_class.new(first_name: "Jan", last_name: "Kowalski", email: "jan@wp.pl", phone_number: "711001115")
    end
    it "is valid" do
      expect(subject).to be_valid
    end
  end

end