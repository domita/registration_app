require 'rails_helper'

describe Address do

  let(:user) { User.create(first_name: "Jan", last_name: "Kowalski", email: "test@wp.pl") }
  let(:company) { Company.create(name: "Comp") }

  context 'user zip code' do
    subject do
      described_class.new(zip_code: "a41300", addressable: user)
    end
    it "is not valid" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Zip code is invalid")
    end
  end

  context 'user zip code' do
    subject do
      described_class.new(street: "Kolorowa", city: "Warszawa", country: "Poland", zip_code: "41-300", addressable: user)
    end
    it "is valid" do
       expect(subject).to be_valid
    end
  end

  context 'company zip code' do
    subject do
      described_class.new(zip_code: "a41300", addressable: company)
    end
    it "is not valid" do
      expect(subject).to_not be_valid
      expect(subject.errors.full_messages).to include("Zip code is invalid")
    end
  end

  context 'company zip code' do
    subject do
      described_class.new(street: "Kolorowa", city: "Warszawa", country: "Poland", zip_code: "41-300", addressable: company)
    end
    it "is valid" do
       expect(subject).to be_valid
    end
  end

end