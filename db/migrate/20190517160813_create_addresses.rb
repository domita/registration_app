class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :city
      t.string :zip_code
      t.string :country
      t.integer :addressable_id, index: true
      t.string :addressable_type, index: true
      t.timestamps
    end
  end
end
